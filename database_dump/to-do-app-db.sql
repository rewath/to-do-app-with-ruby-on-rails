--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: ramesh; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE schema_migrations OWNER TO ramesh;

--
-- Name: tasks; Type: TABLE; Schema: public; Owner: ramesh; Tablespace: 
--

CREATE TABLE tasks (
    id integer NOT NULL,
    task_name character varying,
    date timestamp without time zone,
    priority integer,
    description text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    done integer
);


ALTER TABLE tasks OWNER TO ramesh;

--
-- Name: tasks_id_seq; Type: SEQUENCE; Schema: public; Owner: ramesh
--

CREATE SEQUENCE tasks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tasks_id_seq OWNER TO ramesh;

--
-- Name: tasks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ramesh
--

ALTER SEQUENCE tasks_id_seq OWNED BY tasks.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ramesh
--

ALTER TABLE ONLY tasks ALTER COLUMN id SET DEFAULT nextval('tasks_id_seq'::regclass);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: ramesh
--

COPY schema_migrations (version) FROM stdin;
20150212160955
20150212190332
\.


--
-- Data for Name: tasks; Type: TABLE DATA; Schema: public; Owner: ramesh
--

COPY tasks (id, task_name, date, priority, description, created_at, updated_at, done) FROM stdin;
3	Visit Church	2015-02-20 18:05:00	1	Visit Catholic church and do some prayers...	2015-02-13 18:06:14.901532	2015-02-13 18:06:58.920847	1
4	Go to PB	2015-02-16 18:06:00	3	Go to PB and buy a pair of shoes	2015-02-13 18:06:52.544205	2015-02-13 18:26:20.190094	1
5	Visit Barapani	2015-02-19 18:26:00	4	Pay a vist to sonil's house at barapani.	2015-02-13 18:27:17.635724	2015-02-13 18:27:17.654928	0
7	sdfasfd	2015-02-14 18:58:00	4	asfasd	2015-02-14 18:59:07.015475	2015-02-14 18:59:07.031872	0
\.


--
-- Name: tasks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ramesh
--

SELECT pg_catalog.setval('tasks_id_seq', 7, true);


--
-- Name: tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: ramesh; Tablespace: 
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (id);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: ramesh; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

