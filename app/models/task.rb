class Task < ActiveRecord::Base
scope:done, -> {where(done:1)}
scope:pending, -> {where(done:0)}
end
