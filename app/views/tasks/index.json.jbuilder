json.array!(@tasks) do |task|
  json.extract! task, :id, :task_name, :date, :priority, :description
  json.url task_url(task, format: :json)
end
